import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.urlContains;

public class GoogleSearch {
    String result = ".srg>.g";
    static WebDriver driver;
    static WebDriverWait wait;


    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 6);
    }

    @AfterClass
    public static void teardown() {
        driver.quit();
    }


    public void followLink(int index) {
        driver.findElements(By.cssSelector(result + " .r")).get(index).click();
    }

    private void serchResults(String nameToSerch) {
        driver.get("https://www.google.com.ua/search?q=" + nameToSerch);
    }

    @Test
    public void followLinkResult(){
        serchResults("Selenium automates browsers");
        followLink(0);
        wait.until(urlContains("http://www.seleniumhq.org/"));

    }

    @Test
    public void testSearchCommonFlow() {
        driver.get("http://google.com/ncr");

        driver.findElement(By.name("q")).clear();
        driver.findElement(By.name("q")).sendKeys("Selenium automates browsers" + Keys.ENTER);

        wait.until(sizeOf(By.cssSelector(result), 10));

        wait.until(textToBePresentInElementLocated(By.cssSelector(result + ":nth-child(1)"), "Selenium automates browsers"));

        followLink(0);

        wait.until(urlContains("http://www.seleniumhq.org/"));
    }

    public static ExpectedCondition<Boolean> sizeOf(final By elementsLocator, final int expectedSize) {
        return new ExpectedCondition<Boolean>() {
            private int listSize;
            private List<WebElement> elements;

            public Boolean apply(WebDriver driver) {
                elements = driver.findElements(elementsLocator);
                listSize = elements.size();
                return listSize == expectedSize;
            }

            public String toString() {
                return String.format("\nsize of list: %s\n to be: %s\n while actual size is: %s\n", elements, expectedSize, listSize);
            }
        };
    }


}
